import requests
import os.path
from pathlib import Path
import urllib.parse


class APICaller(object):
    def __init__(self, domain) -> None:
        self._domain = domain
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0",
            "Content-Type": "application/x-www-form-urlencoded",
        }
        self.session = requests.Session()
        self.noneapi = urllib.parse.urljoin(self._domain, "vulnbank/online/api.php")
        self.xmlapi = urllib.parse.urljoin(self._domain, "vulnbank/online/api.php?xml")

    def login(self) -> requests.Response:
        return self.session.post(
            self.noneapi,
            data="type=user&action=login&username=j.doe&password=password",
            headers=self.headers,
        )

    def get_payload(self, filepath):
        # filepath = os.path.join(Path(__file__).resolve().parent, filepath)
        with open(filepath) as file:
            payload = file.read()
            assert len(payload) > 0, "Can't load payload"
            return payload

    def update_header(self, key, value):
        self.headers[key] = value
